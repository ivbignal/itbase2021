# ITBASE2021

Исходные коды и полезные материалы в рамках курса IT Base 2021 Кванториум "Новатория".

## Полезные материалы

Редактор кода - [Visual Studio Code](https://code.visualstudio.com)

Система контроля версий - [GIT](https://git-scm.com/book/ru/v2/Введение-Установка-Git)

Установка PIPENV на Windows - [PIPENV](https://www.pythontutorial.net/python-basics/install-pipenv-windows/)