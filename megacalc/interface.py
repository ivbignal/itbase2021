from os import system, name
from time import sleep

def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')

def logo():
    print("""
███╗   ███╗ ███████╗  ██████╗   █████╗   ██████╗  █████╗  ██╗       ██████╗
████╗ ████║ ██╔════╝ ██╔════╝  ██╔══██╗ ██╔════╝ ██╔══██╗ ██║      ██╔════╝
██╔████╔██║ █████╗   ██║  ███╗ ███████║ ██║      ███████║ ██║      ██║     
██║╚██╔╝██║ ██╔══╝   ██║   ██║ ██╔══██║ ██║      ██╔══██║ ██║      ██║     
██║ ╚═╝ ██║ ███████╗ ╚██████╔╝ ██║  ██║ ╚██████╗ ██║  ██║ ███████╗ ╚██████╗
╚═╝     ╚═╝ ╚══════╝  ╚═════╝  ╚═╝  ╚═╝  ╚═════╝ ╚═╝  ╚═╝ ╚══════╝  ╚═════╝
    """)

def wellcome():
    for i in range(20, -1, -1):
        clear()
        for j in range(i):
            print()
        logo()
        sleep(0.1)

def getint(name = 'ВВОД', error = False):
    clear()
    logo()
    print()
    if error:
        print('ОШИБКА! Требуется ввести целое число!')
        print()
    r = input(f'Введите целое число "{name}": ')
    try:
        return int(r)
    except Exception:
        return getint(name, True)

def getfloat(name = 'ВВОД', error = False):
    clear()
    logo()
    print()
    if error:
        print('ОШИБКА! Требуется ввести вещественное число!')
        print()
    r = input(f'Введите вещественное число "{name}": ')
    try:
        return float(r)
    except Exception:
        return getfloat(name, True)

def getstr(name = 'ВВОД'):
    clear()
    logo()
    print()
    return input(f'Введите значение "{name}": ')

def out(*args, **kwargs):
    clear()
    logo()
    for st in args:
        print(st)
