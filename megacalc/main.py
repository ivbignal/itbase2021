from interface import wellcome, getint, getfloat, getstr, out

wellcome()
a = getint('A')
b = getfloat('B')
s = getstr('S')
out(f'a = {a}', f'b = {b}', f's = {s}')